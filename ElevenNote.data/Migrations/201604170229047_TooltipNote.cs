namespace ElevenNote.data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TooltipNote : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Note", "TooltipNote", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Note", "TooltipNote");
        }
    }
}
