﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ElevenNote.models.Attributes
{
    internal class MaxDigitsAttribute : ValidationAttribute
    {
        private int Max,
                    Min;

        public MaxDigitsAttribute(int max, int min = 0)
        {
            Max = max;
            Min = min;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!IsValid(value))
            {
                return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));
            }
            return null;
        }

        public override bool IsValid(object value)
        {
            // you could do any custom validation you like
            if (value is int)
            {
                var stringValue = "" + (int)value;
                var length = stringValue.Length;
                if (length >= Min && length <= Max)
                    return true;
            }

            return false;
        }
    }
}
