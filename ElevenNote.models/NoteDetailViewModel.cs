﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevenNote.models
{
    public class NoteDetailViewModel
    {
        public int NoteId { get; set; }

        [Display(Name ="Order of Importance")]
        [Range(1, 10, ErrorMessage = "Please use 0 through 10")]
        public int OrderOfImportance { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        [Range(1, 3, ErrorMessage = "1 Not started, 2 Started, 3 Done")]
        public int Status { get; set; }

        [UIHint("Starred")]
        public bool IsStarred { get; set; }

        public string TooltipNote { get; set; }

        [Display(Name = "Created")]
        public DateTimeOffset CreatedUtc { get; set; }

        [Display(Name = "Modified")]
        public DateTimeOffset? ModifiedUtc { get; set; }
    }
}
