﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevenNote.models
{
    public class NoteListItemViewModel
    {
        public int NoteId { get; set; }

        [Display(Name = "Order of Importance")]
        public int OrderOfImportance { get; set; }
        
        public string Title { get; set; }

        public int Status { get; set; }

        public string TooltipNote { get; set; }

        [Display(Name = "Starred")]
        [UIHint("Starred")]
        public bool IsStarred { get; set; }

        [Display(Name = "Created")]
        public DateTimeOffset CreatedUtc { get; set; }

        public override string ToString() => $"[{NoteId}] {Title}";
    }
}
