﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevenNote.models
{
    public class ContactFormViewModel
    { 
        [Required, Display(Name = "Your name")]
        public string FormName { get; set; }

        [Required, Display(Name = "Recipients email"), EmailAddress]
        public string FormEmail { get; set; }

        [Required]
        public string Message { get; set; }
    }
}
