﻿using ElevenNote.models.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ElevenNote.models
{
    public class NoteCreateViewModel
    {
        [Required]
        [MinLength(2, ErrorMessage = "Please enter at least 2 characters")]
        [MaxLength(128)]
        public string Title { get; set; }

        [Required]
        [MaxLength(8000, ErrorMessage ="The max characters is 8000")]
        public string Content { get; set; }

        [Required]
        [Range(1, 10, ErrorMessage = "Please make it 1 through 10")]
        [Display(Name = "Order of Importance")]
        public int OrderOfImportance { get; set; }

        [Required]
        public string TooltipNote { get; set; }

        [Required]
        [Range(1,3, ErrorMessage = "1 Not started, 2 Started, 3 Done")]
        [Display(Name = "Status Code")]
        public int Status { get; set; }
    }
}